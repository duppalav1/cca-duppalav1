FROM node:12

WORKDIR /usr/app-src
COPY package*.json ./

RUN npm install

COPY . .

RUN echo "Creating a Docker image by duppalav1@udayton.edu"

CMD ["npm", "start"]
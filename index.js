const express = require('express');

const app = express();
const cors = require('cors')

app.use(cors())


const MongoClient = require("mongodb").MongoClient;
const mongourl = "mongodb+srv://cca-duppalav1:duppalav1@cca-duppalav1.kzzvn.mongodb.net/cca-labs?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err=>{
    if(err) throw err;
    console.log("Connected to the MongoDB Cluster");
})

var port = process.env.PORT || 8080;
const http = require('http').Server(app).listen(port);
console.log(`Express server is running on port : ${port}`)

app.use(express.static('static'))
app.use(express.urlencoded({extended:false}))
// app.listen(port, () =>
//     console.log(`HTTP Server with Express.js is listening on port : ${port}`))
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/cca-form.html');
})  

let fields = {
    _id : false,
    zips: true,
    city: true,
    state_id: true,
    state_name: true,
    county_name: true,
    timezone: true
};

app.get('/uscities-search', (req, res) => {
    res.send("US City Search by Vamsi Duppala");
})  

app.get('/uscities-search/:zips(\\d{1,5})', (req, res) => {
    const db = dbClient.db();
    // res.send(req.url + req.params.zips);
    let zipRegEx = new RegExp(parseInt(req.params.zips));
    const cursor = db.collection("uscities").find({zips:zipRegEx}).project(fields);
    cursor.toArray(function(err, results){
        console.log(results);
        res.send(results);
    })
})  

app.get('/uscities-search/:city', (req, res) => {
    const db = dbClient.db();
    // res.send(req.url + req.params.zips);
    let cityRegEx = new RegExp(req.params.city, "i");
    const cursor = db.collection("uscities").find({city:cityRegEx}).project(fields);
    cursor.toArray(function(err, results){
        console.log(results);
        res.send(results);
    })
})
  

app.get('/echo.php', function(req, res){
    var data = req.query.data
    res.send(data)
})
app.post('/echo.php', function(req, res){
    var data = req.body['data']
    res.send(data)
})


app.get("/chat", (req, res) => {
    res.sendFile(__dirname + "/static/chatclient/index.html")
})


const io = require('socket.io')(http);
console.log("socket.io server is running on port " + port)


io.on("connection", (socketclient)=>{
    console.log("A new client is connected!");
    socketclienthandler(socketclient);  
})

function socketclienthandler(socketclient){
    socketclient.on("message", (data)=>{
        console.log('Data from a client: ' + data)
        io.emit("message", `${socketclient.id} says: ${data}`);
    })

    socketclient.on("typing", ()=>{
        console.log(`${socketclient.id} is typing ...`)
        socketclient.broadcast.emit("typing", `${socketclient.id} is typing ...`);
    })

    var onlineClients = Object.keys(io.sockets.sockets).length
    var welcomemessage = `${socketclient.id} is connected! Number of connected clients: ${onlineClients}`;
    console.log(welcomemessage);
    io.emit("online", welcomemessage);

    socketclient.on("disconnect", ()=>{
        var onlineClients = Object.keys(io.sockets.sockets).length
        var byemessage = `${socketclient.id} is disconnected! Number of connected clients: ${onlineClients}`;
        console.log(byemessage);
        io.emit("online", byemessage);
    })
}
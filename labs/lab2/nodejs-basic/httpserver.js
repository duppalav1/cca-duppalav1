const fs = require("fs"), url = require('url');
const querystring = require('querystring');

var http = require("http");
var server = http.createServer(httphandler);
var port = process.env.PORT || 8080;

server.listen(port);
console.log("httpserver is running at port:" + port);

function httphandler(request,response){
    console.log("Get a Http Request: "+ request.url);
    var fullpath = url.parse(request.url,true);
    if(fullpath.pathname === "/echo.php"){
        if(request.method == "GET"){
            var query = querystring.parse(querystring.stringify(fullpath.query));
            response.writeHead(200, {'Content-Type':'text/html'}); 
            response.write(query.data);
            return response.end();
        }
        else if (request.method == "POST"){
            let postdata = '';
            request.on('data', (chunk) => {
                postdata += chunk;
            });
            request.on('end', () => {
                postdata = querystring.parse(postdata);
                response.writeHead(200, {'Content-Type':'text/html'}); 
                response.write(postdata.data);
                return response.end();
            });
        }
        return;
    }
    var localfile = ".." + fullpath.pathname;
    console.log("Debug: Server's local filename = " + localfile);
    fs.readFile(localfile, (error, filecontent) => {
        if(error){
            response.writeHead(404);
            return response.end(fullpath.pathname + " is not found on this server.");
        }
        response.writeHead(200, {'Content-Type':'text/html'});
        response.write(filecontent);
        return response.end();
        
    });
}